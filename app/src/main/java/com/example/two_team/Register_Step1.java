package com.example.two_team;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class Register_Step1 extends AppCompatActivity {
    private Spinner mSex_spr;
    private Spinner mIntensity_spr;
    private TextView mBirthday_TV;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private Button mSignup_Btn;
    private TextView mSignin_TV;
    private String user_email, user_password;
    private EditText mUsername_ET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_step1);

        mUsername_ET = (EditText) findViewById(R.id.rusername_ET);
        mSex_spr = (Spinner) findViewById(R.id.sex_spr);
        mIntensity_spr = (Spinner) findViewById(R.id.intensity_spr);
        mBirthday_TV = (TextView) findViewById(R.id.rbirthday_TV);
        mSignup_Btn = (Button) findViewById(R.id.signup_Btn_step2);
        mSignin_TV = (TextView) findViewById(R.id.signin_TV_step2);


        mUsername_ET.setTextColor(Color.WHITE);

/*
        //讀register 傳過來的email & 密碼
        user_email = getIntent().getStringExtra("email");
        user_password = getIntent().getStringExtra("password");

        System.out.println("email+pw" + user_email + user_password);

 */

        Toast.makeText(getApplicationContext(), "Please fill in basic information", Toast.LENGTH_SHORT).show();

        set_spinner_hintText();

        datePicker_initialize();
        set_spinner_color();
        signUp_Btn_Onclick();
        singnIn_TV_OnClick();
    }

    private void singnIn_TV_OnClick() {

        mSignin_TV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext() ,Login.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void set_spinner_hintText() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add("Gentlemen");
        adapter.add("Ladies");
        adapter.add("Select your sex.");

        mSex_spr.setAdapter(adapter);
        mSex_spr.setSelection(adapter.getCount()); //display hint
///////////////////////////////////////
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }

        };

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.add("Beginner");
        adapter2.add("Advanced");
        adapter2.add("Intensive");
        adapter2.add("Professional");
        adapter2.add("Select your intensity.");

        mIntensity_spr.setAdapter(adapter2);
        mIntensity_spr.setSelection(adapter2.getCount()); //display hint
    }

    private void signUp_Btn_Onclick() {

        mSignup_Btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mUsername_ET.getText().toString();
                String sex = mSex_spr.getSelectedItem().toString();
                String intensity = mIntensity_spr.getSelectedItem().toString();
                String birthday = mBirthday_TV.getText().toString();

                //檢查有沒有未填的欄位
                if ((!username.isEmpty()) && (!sex.matches("Select your sex."))
                        &&  (!intensity.matches("Select your intensity."))
                            && (!birthday.isEmpty())){

                    Intent intent = new Intent(getApplicationContext(), Register_Step2.class);
                    intent.putExtra("username", username);
                    intent.putExtra("sex", sex);
                    intent.putExtra("intensity", intensity);
                    intent.putExtra("birthday", birthday);
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), "One or more fields are empty!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void datePicker_initialize() {
        mDateSetListener = new OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                //               datePicker.setCalendarViewShown(false); // 不要以日曆方式顯示

                month = month + 1;

                String date = year + "/" + month + "/" + day;
                mBirthday_TV.setText(date);
                mBirthday_TV.setTextColor(Color.WHITE);
            }
        };

        mBirthday_TV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Register_Step1.this,
                        mDateSetListener,
                        year, month, day);

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

                datePickerDialog.show();
            }
        });
    }

    private void set_spinner_color() {

        mSex_spr.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mIntensity_spr.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}

/*
個人資料就
1. email
2. 生日
3. 強度
4. 性別
 */
