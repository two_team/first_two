package com.example.two_team;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class FindFriend extends AppCompatActivity {

    private ImageButton mSearchBtn;
    private EditText mSearchET;
    private RecyclerView mSearchResultRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);

        //利用findViewById取得在Layout中設定好的UI元件
        RecyclerView mSearchResultRV = findViewById(R.id.search_result_RV);
        mSearchResultRV.setHasFixedSize(true); //設為true，RecyclerView的大小就不會因adapter內容的更改而改變(並不代表RecyclerView大小固定)
        mSearchResultRV.setLayoutManager(new LinearLayoutManager(this)); //LinearLayoutManager負責決定RecyclerView是水平列表還是垂直列表的關鍵

        ImageButton mSearchBtn = findViewById(R.id.search_Btn);
        final EditText mSearchET = findViewById(R.id.search_ET);


        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchBoxInput = mSearchET.getText().toString();
                SearchPeopleAndFriend(searchBoxInput);
            }
        });
    }

    private void SearchPeopleAndFriend(String searchBoxInput) {
    }
}
