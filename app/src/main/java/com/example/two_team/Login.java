package com.example.two_team;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Login extends AppCompatActivity {

    //Email password part
    private EditText mEmailET;
    private EditText mPasswordET;
    private Button mSigninBtn;
    private TextView mForgetTV;
    private TextView mRegisterTV;

    //Google part
    private SignInButton mGoogleBtn;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;

    //Facebook part
    private CallbackManager mCallbackManager;
    private LoginButton mLoginButton;
    private static final String TAG = "FacebookAuthentication";
    private boolean isLoggedIn;

    //Firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private DatabaseReference FIREBASEDATABASE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //Receive values from previous activity
        final String username = getIntent().getStringExtra("username");
        final String sex = getIntent().getStringExtra("sex");
        final String intensity = getIntent().getStringExtra("intensity");
        final String birthday = getIntent().getStringExtra("birthday");

        //Email Password sign in
        mEmailET = findViewById(R.id.email_ET);
        mPasswordET = findViewById(R.id.password_ET);
        mForgetTV = findViewById(R.id.forget_TV);
        mRegisterTV = findViewById(R.id.register_TV);
        mSigninBtn = findViewById(R.id.signin_Btn);

        //Forget password
        mForgetTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,ResetPassword.class));
            }
        });

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
                if(mFirebaseUser != null){
                    Toast.makeText(Login.this,"You are logged in",Toast.LENGTH_SHORT).show();

                    redirect_to_mainActivity();
                }
                else{
                    Toast.makeText(Login.this,"Please Login",Toast.LENGTH_SHORT).show();
                }
            }
        };


        mSigninBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEmailET.getText().toString();
                String password = mPasswordET.getText().toString();
                if (email.isEmpty()){
                    mEmailET.setError("Please enter email");
                    mEmailET.requestFocus();
                }
                else if (password.isEmpty()){
                    mPasswordET.setError("Please enter your password");
                    mPasswordET.requestFocus();
                }
                else if (email.isEmpty() && password.isEmpty()){
                    Toast.makeText(Login.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else if (!(email.isEmpty() && password.isEmpty())){
                    mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()){
                                Toast.makeText(Login.this,"Login Error, Please Login Again",Toast.LENGTH_SHORT).show();
                            }
                            else {
                                String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                FIREBASEDATABASE = FirebaseDatabase.getInstance().getReference().child("Account").child(UID);
                                FIREBASEDATABASE.child("username").setValue(username);
                                FIREBASEDATABASE.child("intensity").setValue(intensity);
                                FIREBASEDATABASE.child("sex").setValue(sex);
                                FIREBASEDATABASE.child("birthday").setValue(birthday);

                                Intent intToHome = new Intent(Login.this,MainActivity.class);
                                startActivity(intToHome);
                                finish();
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(Login.this,"Error Occurred!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        mRegisterTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intSignUp = new Intent(Login.this, Register_Step1.class);
                startActivity(intSignUp);
            }
        });



        //FB sign in
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        isLoggedIn = accessToken != null && !accessToken.isExpired();

        //檢查用戶是否已經login
        if (isLoggedIn){

            redirect_to_mainActivity();

        }else {

            AppEventsLogger.activateApp(getApplication());
            mLoginButton = findViewById(R.id.fb_Btn);
            mCallbackManager = CallbackManager.Factory.create();
            mLoginButton.setReadPermissions("email");

            mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d(TAG, "facebook:onSuccess:" + loginResult);
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "facebook:onCancel");
                }

                @Override
                public void onError(FacebookException error) {
                    Log.d(TAG, "facebook:onError", error);
                }
            });

        }


        mGoogleBtn = findViewById(R.id.google_Btn);
        mAuth = FirebaseAuth.getInstance();
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });


    }

    private void handleFacebookAccessToken(AccessToken Ftoken) {
        Log.d(TAG, "handleFacebookAccessToken:" + Ftoken);
        AuthCredential credential = FacebookAuthProvider.getCredential(Ftoken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "sign in with credential:success");
                            FirebaseUser fuser = mAuth.getCurrentUser();
                            FB_updateUI(fuser);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "sign in with credential:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            FB_updateUI(null);
                        }

                        // ...
                    }
                });
    }



    //facebook update
    private void FB_updateUI(FirebaseUser faceuser) {
        redirect_to_mainActivity();
    }

    //登入成功，轉到主畫面
    private void redirect_to_mainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }


    //Google sign in
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount gaccount = task.getResult(ApiException.class);
                Toast.makeText(Login.this, "Signed In Successfully",Toast.LENGTH_SHORT).show();
                FirebaseGoogleAuth(gaccount);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(Login.this, "Signed In Failed",Toast.LENGTH_SHORT).show();
                FirebaseGoogleAuth(null);
                // ...
            }
        }
    }

    private void FirebaseGoogleAuth(GoogleSignInAccount gacct) {
        AuthCredential authCredential = GoogleAuthProvider.getCredential(gacct.getIdToken(), null);
        mAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Toast.makeText(Login.this, "Successful",Toast.LENGTH_SHORT).show();
                    FirebaseUser guser = mAuth.getCurrentUser();
                    google_updateUI(guser);
                }
                else {
                    Toast.makeText(Login.this, "Failed",Toast.LENGTH_SHORT).show();
                    google_updateUI(null);
                }
            }
        });
    }

    //google update
    private void google_updateUI(FirebaseUser googleuser){
        GoogleSignInAccount gaccount = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (gaccount != null){
            String GpersonName = gaccount.getDisplayName();
            String GpersonGivenName = gaccount.getGivenName();
            String GpersonFamilyName = gaccount.getFamilyName();
            String GpersonEmail = gaccount.getEmail();
            String GpersonId = gaccount.getId();
            Uri GpersonPhoto = gaccount.getPhotoUrl();

            Toast.makeText(Login.this, GpersonFamilyName + GpersonEmail,Toast.LENGTH_SHORT).show();
            Log.d("Google acc. name:", GpersonFamilyName + GpersonGivenName);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);

        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        google_updateUI(currentUser);
    }
}
