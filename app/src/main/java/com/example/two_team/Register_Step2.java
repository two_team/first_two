package com.example.two_team;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Register_Step2 extends AppCompatActivity {
    private EditText mREmailET;
    private EditText mRPasswordET;
    private EditText mConfirmET;
    private Button mSignupBtn;
    private TextView mSigninTV;

    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabase;
    private String UID;

    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.register_step2);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mREmailET = findViewById(R.id.remail_ET);
        mRPasswordET = findViewById(R.id.rpassword_ET);
        mConfirmET = findViewById(R.id.confirm_ET);
        mSigninTV = findViewById(R.id.signin_TV);
        mSignupBtn = findViewById(R.id.signup_Btn);

        mREmailET.setTextColor(Color.WHITE);
        mRPasswordET.setTextColor(Color.WHITE);
        mConfirmET.setTextColor(Color.WHITE);

        mSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mREmailET.getText().toString();
                String password = mRPasswordET.getText().toString();
                String confirm = mConfirmET.getText().toString();

                if (email.isEmpty()){
                    mREmailET.setError("Please enter email");
                    mREmailET.requestFocus();
                }
                else if (password.isEmpty()){
                    mRPasswordET.setError("Please enter your password");
                    mRPasswordET.requestFocus();
                }
                else if (confirm.isEmpty()){
                    mConfirmET.setError("Please enter your password again");
                    mConfirmET.requestFocus();
                }
                else if (email.matches("")||password.matches("")||confirm.matches("")){
                    Toast.makeText(Register_Step2.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else if(!(password.equals(confirm))){
                    Toast.makeText(getApplicationContext(), "Password Doesn't Match", Toast.LENGTH_LONG).show();
                }

                else if (password.equals(confirm)){
                    mFirebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(Register_Step2.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()){
                                new AlertDialog.Builder(Register_Step2.this)
                                        .setMessage("Register Unsuccessful, Please Try Again")
                                        .setPositiveButton("OK", null)
                                        .show();

                            }
                            else{
                                UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                // 把用戶資料 寫入資料庫

                                Intent intent = getIntent();

                                mDatabase.child("User").child(UID).child("Username").setValue(intent.getStringExtra("username"));
                                mDatabase.child("User").child(UID).child("Sex").setValue(intent.getStringExtra("sex"));
                                mDatabase.child("User").child(UID).child("Intensity").setValue(intent.getStringExtra("intensity"));
                                mDatabase.child("User").child(UID).child("Birthday").setValue(intent.getStringExtra("birthday"));





                                Intent intStep2 = new Intent(Register_Step2.this,MainActivity.class);
                                startActivity(intStep2);
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(Register_Step2.this,"Error Occurred!",Toast.LENGTH_SHORT).show();
                }
            }
        });
        mSigninTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Register_Step2.this,Login.class);
                startActivity(i);
            }
        });
    }
}
